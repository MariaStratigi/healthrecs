/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import DatasetCreation.DatasetCreation;
import Variables.Variables;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.util.HashMap;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

/**
 *
 * @author mariastr
 */
public class TabDemo {

    final static String PATHS = "File Paths";
    final static String DOCUMENTS = "Document Corpus";
    final static String PATIENTS = "Patients Info";
    final static String RATINGS = "Ratings";
    private Variables var;

    JTabbedPane tabbedPane;
    HashMap<String, JPanel> map;

    public TabDemo() {
        map = new HashMap<>();
    }

    public void addComponentToPane(Container pane, JFrame frame) {
        tabbedPane = new JTabbedPane();

        JPanel card1 = new FilePaths();
        JPanel card2 = new DocumentsVar();
        JPanel card3 = new PatientsVar();
        JPanel card4 = new RatingsVar();

        map.put("paths", card1);
        map.put("docs", card2);
        map.put("patients", card3);
        map.put("ratings", card4);

        tabbedPane.addTab(PATHS, card1);
        tabbedPane.addTab(DOCUMENTS, card2);
        tabbedPane.addTab(PATIENTS, card3);
        tabbedPane.addTab(RATINGS, card4);
        pane.add(tabbedPane, BorderLayout.CENTER);
        JButton butt = new JButton("OK");
        //butt.setSize(23, 60);
//        butt.setPreferredSize(new Dimension(0, 40));
        butt.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if(OKActionPerformed(evt)){
                    JOptionPane.showMessageDialog(tabbedPane, "All Done.");
                    frame.dispose();
                }
            }
        });
        pane.add(butt, BorderLayout.SOUTH);
    }

    private boolean OKActionPerformed(java.awt.event.ActionEvent evt) {
        var = new Variables();
        FilePaths card1 = (FilePaths) map.get("paths");
        if (!card1.errorCheck()) {
            JOptionPane.showMessageDialog(this.tabbedPane, "Please fill all input in tab 'File Paths'");
            return false;
        }

        DocumentsVar docs = (DocumentsVar) map.get("docs");
        if (!docs.errorCheck()) {
            JOptionPane.showMessageDialog(this.tabbedPane, "Please fill all input in tab 'Document Corpus'");
            return false;
        } 
        PatientsVar pat = (PatientsVar) map.get("patients");
        if (!pat.errorCheck()) {
            JOptionPane.showMessageDialog(this.tabbedPane, "Please check all input in tab 'Patients Info'");
            return false;
        }
        RatingsVar rat = (RatingsVar) map.get("ratings");
        if (!rat.errorCheck()) {
            JOptionPane.showMessageDialog(this.tabbedPane, "Please check all input in tab 'Ratings'");
            return false;
        }

        var.setOntology(card1.getOntologyPath());
        var.setAdmissions(card1.getAdmissionPath());
        var.setPatients(card1.getPatientPath());
        var.setStopwords(card1.getStopWordsPath());
        var.setOutput(card1.getOutputPath());

        var.setNumRated(docs.getRatedDocs());
        var.setNumDocs(docs.getNumDocs());
        var.setNumKeywords(docs.getKeywords());

        var.setGroupSparse(pat.getSparse());
        var.setGroupRegular(pat.getRegular());
        var.setGroupDedicated(pat.getDedicated());

        var.setSparseMin(pat.getSparseMin());
        var.setSparseMax(pat.getSparseMax());

        var.setRegularMin(pat.getRegularMin());
        var.setRegularMax(pat.getRegularMax());

        var.setDedicatedMin(pat.getDedicatedMin());
        var.setDedicatedMax(pat.getDedicatedMax());

        var.setHealthRelevant(pat.getRelevant());
        var.setNoHealthRelevant(pat.getNoRelevant());

        var.setScore1(rat.getScore1());
        var.setScore2(rat.getScore2());
        var.setScore3(rat.getScore3());
        var.setScore4(rat.getScore4());
        var.setScore5(rat.getScore5());

        DatasetCreation data = new DatasetCreation(var);
        data.createDataset();
        
        return true;
    }
}
