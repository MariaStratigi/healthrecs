/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Variables;

/**
 *
 * @author mariastr
 */
public class Variables {
    
    private String stopwords = "";
    private String ontology = "";
    private String admissions= "";
    private String patients= "";
    private String output="";
    private int numDocs;
    private int numKeywords;
    private int numRated;
    private int groupSparse;
    private int groupRegular;
    private int groupDedicated;
    private int sparseMin;
    private int sparseMax;
    private int regularMin;
    private int regularMax;
    private int dedicatedMin;
    private int dedicatedMax;
    private int healthRelevant;
    private int noHealthRelevant;
    private int score1;
    private int score2;
    private int score3;
    private int score4;
    private int score5;

    public void setStopwords(String stopwords) {
        this.stopwords = stopwords;
    }

    public void setOntology(String ontology) {
        this.ontology = ontology;
    }

    public void setAdmissions(String admissions) {
        this.admissions = admissions;
    }

    public void setPatients(String patients) {
        this.patients = patients;
    }

    public String getStopwords() {
        return stopwords;
    }

    public String getOntology() {
        return ontology;
    }

    public String getAdmissions() {
        return admissions;
    }

    public String getPatients() {
        return patients;
    }

    public void setNumDocs(int numDocs) {
        this.numDocs = numDocs;
    }

    public void setNumKeywords(int numKeywords) {
        this.numKeywords = numKeywords;
    }

    public void setNumRated(int numRated) {
        this.numRated = numRated;
    }

    public void setGroupSparse(int groupSparse) {
        this.groupSparse = groupSparse;
    }

    public void setGroupRegular(int groupRegular) {
        this.groupRegular = groupRegular;
    }

    public void setGroupDedicated(int groupDedicated) {
        this.groupDedicated = groupDedicated;
    }

    public void setSparseMin(int sparseMin) {
        this.sparseMin = sparseMin;
    }

    public void setSparseMax(int sparseMax) {
        this.sparseMax = sparseMax;
    }

    public void setRegularMin(int regularMin) {
        this.regularMin = regularMin;
    }

    public void setRegularMax(int regularMax) {
        this.regularMax = regularMax;
    }

    public void setDedicatedMin(int dedicatedMin) {
        this.dedicatedMin = dedicatedMin;
    }

    public void setDedicatedMax(int dedicatedMax) {
        this.dedicatedMax = dedicatedMax;
    }

    public void setHealthRelevant(int healthRelevant) {
        this.healthRelevant = healthRelevant;
    }

    public void setNoHealthRelevant(int noHealthRelevant) {
        this.noHealthRelevant = noHealthRelevant;
    }

    public void setScore1(int score1) {
        this.score1 = score1;
    }

    public void setScore2(int score2) {
        this.score2 = score2;
    }

    public void setScore3(int score3) {
        this.score3 = score3;
    }

    public void setScore4(int score4) {
        this.score4 = score4;
    }

    public void setScore5(int score5) {
        this.score5 = score5;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public int getNumDocs() {
        return numDocs;
    }

    public int getNumKeywords() {
        return numKeywords;
    }

    public int getNumRated() {
        return numRated;
    }

    public int getGroupSparse() {
        return groupSparse;
    }

    public int getGroupRegular() {
        return groupRegular;
    }

    public int getGroupDedicated() {
        return groupDedicated;
    }

    public int getSparseMin() {
        return sparseMin;
    }

    public int getSparseMax() {
        return sparseMax;
    }

    public int getRegularMin() {
        return regularMin;
    }

    public int getRegularMax() {
        return regularMax;
    }

    public int getDedicatedMin() {
        return dedicatedMin;
    }

    public int getDedicatedMax() {
        return dedicatedMax;
    }

    public int getHealthRelevant() {
        return healthRelevant;
    }

    public int getNoHealthRelevant() {
        return noHealthRelevant;
    }

    public int getScore1() {
        return score1;
    }

    public int getScore2() {
        return score2;
    }

    public int getScore3() {
        return score3;
    }

    public int getScore4() {
        return score4;
    }

    public int getScore5() {
        return score5;
    }

    public String getOutput() {
        return output;
    }
    
    
    
    
}
