/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Patients;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author user
 */
public class Categories {

   
    private HashMap<String, Integer> catCount;
    private Map<String, Patient> patients;
    private Map<String, String> categoryLex;

    public Categories(Map<String, Patient> pat, Map<String, String> categoryLex) {
        this.patients = pat;
        catCount = new HashMap<>();
        this.categoryLex = categoryLex;
        calcCategories();
    }

    private void calcCategories() {
        for (Patient p : patients.values()) {
            Map<String, String> ill = p.getIllness();
            for (String str : ill.keySet()) {
                String code;
                if(categoryLex.containsKey(str)){
                    code = categoryLex.get(str);
                }else{
                    code = null;
                    System.out.println(str);
                }
                

                if (catCount.containsKey(code)) {
                    int count = catCount.get(code);
                    count++;
                    catCount.put(code, count);
                } else {
                    catCount.put(code, 1);
                }
            }
        }
    }

    public void setCategoryLex(Map<String, String> categoryLex) {
        this.categoryLex = categoryLex;
    }


    public List<String> getTopKCategories() {
        List<String> top = new ArrayList<>();
        catCount = sortByValues(catCount);
//        printCategories(catCount, "entries");
        for (String s : catCount.keySet()) {
            top.add(s);
        }

        return top;
    }

    private static HashMap sortByValues(HashMap map) {
        List list = new LinkedList(map.entrySet());
        // Defined Custom Comparator here
        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o2)).getValue())
                        .compareTo(((Map.Entry) (o1)).getValue());
            }
        });

        // Here I am copying the sorted list in HashMap
        // using LinkedHashMap to preserve the insertion order
        HashMap sortedHashMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedHashMap.put(entry.getKey(), entry.getValue());
        }
        return sortedHashMap;
    }

    public void printCategories(HashMap<String, Integer> map, String file) {
        try {
            PrintWriter writer = new PrintWriter(file+".txt", "UTF-8");
            for(String str : map.keySet()){
                writer.println(str + "\t" + map.get(str));
            }
            writer.close();
        } catch (IOException e) {
            // do something
        }
    }
}
