/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Patients;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import ratings.Ratings;

/**
 *
 * @author user
 */
public class Patient {

    private final String id;
    private String gender;
    private String dob;
    private String race;
    private String maritalStat;
    private String lang;
    private double poverty;

    private Ratings ratings;

    private String group;

    private Map<String, String> illness;
    private List<String> categories;
    private Map<String, Integer> ratingPerCat;
    

    public Patient(String id) {
        this.id = id;
        illness = new HashMap<>();
        categories = new ArrayList<>();
        group = "";
        ratingPerCat = new HashMap<>();
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public void setMaritalStat(String maritalStat) {
        this.maritalStat = maritalStat;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public void setPoverty(String poverty) {
        try {
            this.poverty = Double.parseDouble(poverty);
        } catch (NumberFormatException ex) {
            this.poverty = 0.0;
            System.out.println("Error: The patient with id '" + this.id + "' has error in poverty");
        }
    }

    public String getId() {
        return id;
    }

    public String getGender() {
        return gender;
    }

    public String getDob() {
        return dob;
    }

    public String getRace() {
        return race;
    }

    public String getMaritalStat() {
        return maritalStat;
    }

    public String getLang() {
        return lang;
    }

    public double getPoverty() {
        return poverty;
    }

    public void addIllness(String code, String desc) {
        if (illness.containsKey(code)) {
            String tmp = illness.get(code);
            if (!tmp.equalsIgnoreCase(desc)) {
                System.out.println("Error in code " + code + " with it's description");
            }
            return;
        }

        illness.put(code, desc);
        String tmp = checkCategory(code);
        if (!categories.contains(tmp)) {
            categories.add(tmp);
        }
    }

    public String getGroup() {
        return group;
    }

    public Map<String, String> getIllness() {
        return illness;
    }

    public void setGroup(String group, int rmin, int rmax) {
        ratings = new Ratings(group, rmin, rmax);
        this.group = group;
    }

    private String checkCategory(String code) {
        if (code.contains(".")) {
//            System.out.println("code: " + code);
            String[] tmp = code.split("\\.");
            return tmp[0];
        }
        return code;
    }

    public boolean hasMoreRatings(){
        return this.ratings.hasMoreRatings();
    }
    
    public String getACategory(){
            int min = 0;
            int max = this.categories.size();
            int ran = ThreadLocalRandom.current().nextInt(min, max);
            return categories.get(ran);
    }
    
    public boolean addRating(Map<Integer, Set<String>> doc,int threshold){
        int min = 0;
        int max = doc.size();
        List<Integer> keys = new ArrayList<>();
        keys.addAll(doc.keySet());
        int i = 0;
        while(true){
            int ran = ThreadLocalRandom.current().nextInt(min, max);
            int id = keys.get(ran);
            if(id > threshold){
                continue;
            }
            if(!ratings.isDocumentRated(id)){
                if(ratings.addDocumentForRating(id)){
                    ratings.AddToTotalRatings();
                    return true;
                }
            }
            if(i == 500){
//                System.out.println("Avoiding eternal loop");
                return false;
            }
            i++;
        }
    }
    
    public int getTotalRatings(){
        return ratings.getTotalRatings();
    }
    
    public boolean FillRatings(int r){
        if(this.ratings.hasToFillRatings()){
            this.ratings.fillRating(r);
            return true;
        }
        return false;
    }
    
    public int getNumOfRatings(){
        return this.ratings.getNumOfRatings();
    }
    
    public int getNumOfCategories(){
        return categories.size();
    }
    
    public boolean categoryExist(String cat){
        return this.categories.contains(cat);
    }

    public Ratings getRatings() {
        return ratings;
    }
    
    public void setCategorySpan(int in, int out){
        this.ratings.setCategoriesSpan(in, out);
    }
    
    public boolean hasMoreInRatings(){
        return this.ratings.canAddRatingIn();
    }
    public boolean hasMoreOutRatings(){
        return this.ratings.canAddRatingOut();
    }
    
    public void adjustInCounter(){
        this.ratings.adjustInCounter();
    }
    
    public void adjustOutCounter(){
        this.ratings.adjustOutCounter();
    }
    
    public void addRatingPerCat(String cat){
        if(ratingPerCat.containsKey(cat)){
            int i = ratingPerCat.get(cat);
            i++;
            ratingPerCat.put(cat, i);
        }else{
            ratingPerCat.put(cat, 1);
        }
    }

    public Map<String, Integer> getRatingPerCat() {
        return ratingPerCat;
    }
    
    
}
