/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Documents;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class FixFormat {

    private String file;

    public FixFormat(String file) {
        this.file = file;
    }
    
    public FixFormat(){
        
    }

    public void setFile(String file) {
        this.file = file;
    }

    public void fixFormatFile() {
        List<String> lines = new ArrayList<>();
        try {
            BufferedReader in = new BufferedReader(new FileReader(file));
            String line;
            
            while ((line = in.readLine()) != null) {
                if (line.equalsIgnoreCase(" ")) {
                    //System.out.println("Found it!");
                } else {
                    //System.out.println("!");
                    lines.add(line);
                }
            }
            in.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FixFormat.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FixFormat.class.getName()).log(Level.SEVERE, null, ex);
        }
        String [] f = file.split("\\.");
        System.out.println("file: " + file);
        System.out.println("f: " + f.length);
        String fixedfile = f[0]+"_fixed.txt";
        try {
            PrintWriter writer = new PrintWriter(fixedfile, "UTF-8");
            for(String s : lines){
                if(s.startsWith("$")){
                    writer.println("======================================================================");
                }
                writer.println(s);
            }
            writer.close();
        } catch (IOException e) {
            // do something
        }
    }

}
